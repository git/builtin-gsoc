-p::
	Generate patch (see section on generating patches)

-u::
	Synonym for "-p".

-U<n>::
	Shorthand for "--unified=<n>".

--unified=<n>::
	Generate diffs with <n> lines of context instead of
	the usual three. Implies "-p".

--raw::
	Generate the raw format.

--patch-with-raw::
	Synonym for "-p --raw".

--stat[=width[,name-width]]::
	Generate a diffstat.  You can override the default
	output width for 80-column terminal by "--stat=width".
	The width of the filename part can be controlled by
	giving another width to it separated by a comma.

--numstat::
	Similar to \--stat, but shows number of added and
	deleted lines in decimal notation and pathname without
	abbreviation, to make it more machine friendly.  For
	binary files, outputs two `-` instead of saying
	`0 0`.

--shortstat::
	Output only the last line of the --stat format containing total
	number of modified files, as well as number of added and deleted
	lines.

--summary::
	Output a condensed summary of extended header information
	such as creations, renames and mode changes.

--patch-with-stat::
	Synonym for "-p --stat".

-z::
	NUL-line termination on output.  This affects the --raw
	output field terminator.  Also output from commands such
	as "git-log" will be delimited with NUL between commits.

--name-only::
	Show only names of changed files.

--name-status::
	Show only names and status of changed files.

--color::
	Show colored diff.

--no-color::
	Turn off colored diff, even when the configuration file
	gives the default to color output.

--color-words::
	Show colored word diff, i.e. color words which have changed.

--no-renames::
	Turn off rename detection, even when the configuration
	file gives the default to do so.

--check::
	Warn if changes introduce trailing whitespace
	or an indent that uses a space before a tab.

--full-index::
	Instead of the first handful characters, show full
	object name of pre- and post-image blob on the "index"
	line when generating a patch format output.

--binary::
	In addition to --full-index, output "binary diff" that
	can be applied with "git apply".

--abbrev[=<n>]::
	Instead of showing the full 40-byte hexadecimal object
	name in diff-raw format output and diff-tree header
	lines, show only handful hexdigits prefix.  This is
	independent of --full-index option above, which controls
	the diff-patch output format.  Non default number of
	digits can be specified with --abbrev=<n>.

-B::
	Break complete rewrite changes into pairs of delete and create.

-M::
	Detect renames.

-C::
	Detect copies as well as renames.  See also `--find-copies-harder`.

--diff-filter=[ACDMRTUXB*]::
	Select only files that are Added (`A`), Copied (`C`),
	Deleted (`D`), Modified (`M`), Renamed (`R`), have their
	type (mode) changed (`T`), are Unmerged (`U`), are
	Unknown (`X`), or have had their pairing Broken (`B`).
	Any combination of the filter characters may be used.
	When `*` (All-or-none) is added to the combination, all
	paths are selected if there is any file that matches
	other criteria in the comparison; if there is no file
	that matches other criteria, nothing is selected.

--find-copies-harder::
	For performance reasons, by default, `-C` option finds copies only
	if the original file of the copy was modified in the same
	changeset.  This flag makes the command
	inspect unmodified files as candidates for the source of
	copy.  This is a very expensive operation for large
	projects, so use it with caution.  Giving more than one
	`-C` option has the same effect.

-l<num>::
	-M and -C options require O(n^2) processing time where n
	is the number of potential rename/copy targets.  This
	option prevents rename/copy detection from running if
	the number of rename/copy targets exceeds the specified
	number.

-S<string>::
	Look for differences that contain the change in <string>.

--pickaxe-all::
	When -S finds a change, show all the changes in that
	changeset, not just the files that contain the change
	in <string>.

--pickaxe-regex::
	Make the <string> not a plain string but an extended POSIX
	regex to match.

-O<orderfile>::
	Output the patch in the order specified in the
	<orderfile>, which has one shell glob pattern per line.

-R::
	Swap two inputs; that is, show differences from index or
	on-disk file to tree contents.

--text::
	Treat all files as text.

-a::
	Shorthand for "--text".

--ignore-space-at-eol::
	Ignore changes in white spaces at EOL.

--ignore-space-change::
	Ignore changes in amount of white space.  This ignores white
	space at line end, and consider all other sequences of one or
	more white space characters to be equivalent.

-b::
	Shorthand for "--ignore-space-change".

--ignore-all-space::
	Ignore white space when comparing lines.  This ignores
	difference even if one line has white space where the other
	line has none.

-w::
	Shorthand for "--ignore-all-space".

--exit-code::
	Make the program exit with codes similar to diff(1).
	That is, it exits with 1 if there were differences and
	0 means no differences.

--quiet::
	Disable all output of the program. Implies --exit-code.

--ext-diff::
	Allow an external diff helper to be executed. If you set an
	external diff driver with gitlink:gitattributes(5), you need
	to use this option with gitlink:git-log(1) and friends.

--no-ext-diff::
	Disallow external diff drivers.

For more detailed explanation on these common options, see also
link:diffcore.html[diffcore documentation].
